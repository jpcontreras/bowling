class BowlingGame < ApplicationRecord
  has_many :frames, dependent: :destroy

  TOTAL_TURNS = 10

  # @return json
  scope :get_format_json, lambda {
    as_json(
      only: %i[id total_score email created_at]
    )
  }

  # @param set integer
  # @param turn_one integer
  # @param turn_two integer
  # @return Frame
  def set_frame(set, turn_one, turn_two)
    # if frames.present?
    #   raise 'Total Turnos Superados' unless frames.last.number_frame < TOTAL_TURNS
    # end
    frame = frames.create!(
      number_frame: set,
      bowls_turn_one: turn_one,
      bowls_turn_two: turn_two,
      state_id: get_state_id(turn_one, turn_two)
    )
    recalculate_scores if frames.count > 1
    frame
  rescue StandardError => e
    raise e
  end

  private

  def recalculate_scores
    total = 0
    frames.each do |frame|
      frame.score = if frame.is_spare?
                      spare_frame(frame)
                    elsif frame.is_strike?
                      strike_frame(frame)
                    else
                      normal_frame(frame)
                    end
      frame.save!
      total += frame.score
    end
    self.total_score = total
    save!
  rescue StandardError => e
    raise e
  end

  # @param turn_one integer
  # @param turn_two integer
  # @return integer
  def get_state_id(turn_one, turn_two)
    if turn_one == Frame::BONUS_VALUE
      State::STRIKE
    elsif (turn_one + turn_two) == Frame::BONUS_VALUE
      State::SPARE
    elsif (turn_one + turn_two).zero?
      State::MISS
    elsif (turn_one + turn_two) <= Frame::BONUS_VALUE
      State::NONE
    end
  rescue StandardError => e
    raise e
  end

  # @param frame Frame
  # @return integer
  def normal_frame(frame)
    (frame.bowls_turn_one + frame.bowls_turn_two)
  end

  # @param frame Frame
  # @return integer
  def strike_frame(frame)
    set_bonus frame, 2
  end

  # @param frame Frame
  # @return integer
  def spare_frame(frame)
    set_bonus frame, 1
  end

  # @param frame Frame
  # @param next_turns integer
  # @return integer
  def set_bonus(frame, next_turns)
    next_number_frame = frame.number_frame + 1
    total = 0
    frame_calc = frames.find_by_number_frame next_number_frame
    if frame_calc.present? && frame.number_frame <= TOTAL_TURNS
      total += Frame::BONUS_VALUE
      if next_turns == 1
        total += frame_calc.bowls_turn_one
      elsif next_turns == 2
        total += frame_calc.total_score
        if frame_calc.is_strike?
          frame_calc = frames.find_by_number_frame(next_number_frame+1)
          total += frame_calc.total_score if frame_calc.present?
        end
      end
    end
    total
  rescue StandardError => e
    raise e
  end
end
