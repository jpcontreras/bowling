class Frame < ApplicationRecord
  belongs_to :bowling_game
  belongs_to :state

  # validates_presence_of :number_frame, :state, :score

  BONUS_VALUE = 10

  def total_score
    bowls_turn_one + bowls_turn_two
  end

  def is_strike?
    state.id == State::STRIKE
  end

  def is_spare?
    state.id == State::SPARE
  end

end
