class State < ApplicationRecord
  STRIKE = 1
  SPARE  = 2
  MISS   = 3
  NONE   = 4
end
