class Api::V1::BowlingGameController < ApplicationController
  def index
    render_default_format json_format(BowlingGame.all), true
  rescue StandardError => e
    render_default_error e, 401
  end

  def show
    render_default_format json_format(BowlingGame.find(params[:id])), true
  rescue StandardError => e
    render_default_error e, 401
  end

  def create
    bowling = BowlingGame.create!(user: params[:user], email: params[:email])
    render_success_format "game created with success!", json_format(bowling)
  rescue StandardError => e
    render_default_error e, 401
  end

  def destroy
    bowling = BowlingGame.find params[:id]
    bowling.destroy!
    render_success_format "game deleted with success!", []
  rescue StandardError => e
    render_default_error e, 401
  end

  def set_frame
    puts params.inspect
    game = BowlingGame.find(params[:id])
    if game.present?
      key = game.frames.present? ? (game.frames.last.number_frame + 1) : 1
      frame = game.set_frame(key, params[:turn_one].to_i, params[:turn_two].to_i)
    end
    render_success_format "Frame created with success!", frame_json_format(frame)
  rescue StandardError => e
    render_default_error e, 401
  end

  def destroy_frame
    game = BowlingGame.find(params[:id])
    if game.present?
      frame = game.frames.find params[:frame_id]
      frame.destroy! if frame.present?
    end
    render_success_format "Frame deleted with success!", []
  rescue StandardError => e
    render_default_error e, 401
  end

  private
  # @param model Frame
  # @return json
  def frame_json_format model
    model.as_json(
      only: %i[id score number_frame bowls_turn_one bowls_turn_two],
      include: {
        state: {
          only: %i[code name]
        }
      }
    )
  end

  # @param model BowlingGame
  # @return json
  def json_format model
    model.as_json(
      only: %i[id total_score email created_at],
      include: {
        frames: {
          only: %i[id score number_frame bowls_turn_one bowls_turn_two],
          include: {
            state: {
              only: %i[code name]
            }
          }
        }
      }
    )
  end
end
