Rails.application.routes.draw do
  root 'home#index'

  get 'home/index'

  namespace :api do
    namespace :v1 do
      resources :bowling_game, only: %i[index show create destroy] do
        collection do
          post ':id/set_frame', action: :set_frame
          delete ':id/destroy_frame/:frame_id', action: :destroy_frame
        end
      end
    end
  end
end
