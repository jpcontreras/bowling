require 'rails_helper'

RSpec.describe BowlingGame, type: :model do

  it 'with ten turns and 0 acerts the total score equal 0' do
    set_frames_test 0, 0, 0
  end

  it 'with score six by every frame, the total score is 60' do
    set_frames_test 3, 3, 60
  end

  it 'with Spare Frame the result is 31' do
    game_scores = [
      { bowls_turn_one: 5, bowls_turn_two: 0 },
      { bowls_turn_one: 7, bowls_turn_two: 3 },
      { bowls_turn_one: 7, bowls_turn_two: 2 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 }
    ]
    set_many_frames(game_scores, 31)
  end


  it 'with Strike Frame the result is 46' do
    game_scores = [
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 7, bowls_turn_two: 3 },
      { bowls_turn_one: 7, bowls_turn_two: 2 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 },
      { bowls_turn_one: 0, bowls_turn_two: 0 }
    ]
    set_many_frames(game_scores, 46)
  end

  it 'with 12 Strike Frames the result is 300' do
    game_scores = [
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 },
      { bowls_turn_one: 10, bowls_turn_two: 0 }
    ]
    set_many_frames(game_scores, 300)
  end

  it 'with 10 pairs of 9 and miss, the result is 90' do
    game_scores = [
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 },
      { bowls_turn_one: 9, bowls_turn_two: 0 }
    ]
    set_many_frames(game_scores, 90)
  end

  it 'with 10 pairs of 5 and spare with a final 5, the result is 150' do
    game_scores = [
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 5, bowls_turn_two: 5 },
      { bowls_turn_one: 0, bowls_turn_two: 5 }
    ]
    set_many_frames(game_scores, 150)
  end


  private

  def set_many_frames(game_scores, result)
    game = BowlingGame.create!(user: 'jpcontreras', email: 'jpcontreras.dev@gmail.com')
    game_scores.each_with_index do |score, key|
      game.set_frame((key + 1), score[:bowls_turn_one], score[:bowls_turn_two])
    end
    expect(game.total_score).to eq result
  end

  def set_frames_test(bowls_turn_one, bowls_turn_two, result)
    total_sets = 0
    game = BowlingGame.create!(user: 'jpcontreras', email: 'jpcontreras.dev@gmail.com')
    while total_sets < BowlingGame::TOTAL_TURNS
      game.set_frame(total_sets + 1, bowls_turn_one, bowls_turn_two)
      total_sets += 1
    end
    expect(game.total_score).to eq result
  end
end
