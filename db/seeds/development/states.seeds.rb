class << self
  def create_state data
    state = State.find_by data
    unless state.present?
      State.create!(data)
    end
  end
end

create_state(id: 1, code: 'X', name: 'Strike')
create_state(id: 2, code: '/', name: 'Spare')
create_state(id: 3, code: '-', name: 'Miss')
create_state(id: 4, code: '', name: 'None')