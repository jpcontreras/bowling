class AddBowlingGameReferenceToFrame < ActiveRecord::Migration[5.1]
  def up
    add_reference :frames, :bowling_game
  end

  def down
    remove_reference :frames, :bowling_game
  end

end
