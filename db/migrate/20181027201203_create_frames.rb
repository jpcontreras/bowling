class CreateFrames < ActiveRecord::Migration[5.1]
  def up
    create_table :frames do |t|
      t.integer :score
      t.integer :number_frame
      t.integer :bowls_turn_one
      t.integer :bowls_turn_two

      t.timestamps
    end
    add_reference :frames, :state, foreign_key: { to_table: :states }
  end

  def down
    remove_reference :frames, :state, foreign_key: { to_table: :states }
    drop_table :frames
  end
end
