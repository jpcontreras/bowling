class CreateBowlingGames < ActiveRecord::Migration[5.1]
  def up
    create_table :bowling_games do |t|
      t.integer :total_score
      t.string :user
      t.string :email

      t.timestamps
    end
  end

  def down
    drop_table :bowling_games
  end
end
